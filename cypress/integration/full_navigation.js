describe("Full app navigation", function () {
  it("Should visit app", function () {
    cy.visit("http://localhost:8080");
    cy.contains("Favorites");
  });
  it("Should type text in the input", function () {
    cy.get("input")
      .should("have.attr", "placeholder", "Search your favorite pokemons...")
      .type("pikachu");
    cy.wait(200);
  });
  it("Should add items to our favorite pokemons", function () {
    cy.get("button").first().click();
  });
  it("Should show pokemon info", function () {
    cy.get("button").contains("Info").click();
    cy.contains("Base experience").should("be.visible");
  });
  it("Should delete items from favorite pokemons", function () {
    cy.get("button").contains("Delete").click();
    cy.contains("Info").should("not.exist");
  });
});
