# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.0] - 2022-02-07

### Added

- CHANGELOG file
- npm-check-updates package to mantain all dependencies updated
- scripts in package.json to execute npm-check-updates updates
- mode:development in webpack file
- state management with react context
- Apollo to search name coincidences while typing in PokeApi GraphQLv1beta
- Tailwind CSS

### Changed

- update all dependencies
- Refactor components structure
- Change pokemon list pagination by autocomplete dropdown

### Fixed

- fix selectedPokemons proptype.
- babel setup to support async await

### Removed

- Emotion
- React class components

## [1.0.1] - 2022-13-07

### Added

- full navigation test with cypress `npm run cy:run` to perform the test and `npm run cy:test` to review it.
- two unit tests for Search component. `npm test`
