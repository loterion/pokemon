const { RESTDataSource } = require('apollo-datasource-rest')

class PokemonAPI extends RESTDataSource {
  constructor() {
    super()
    this.baseURL = 'https://pokeapi.co/api/v2/'
  }
  async getAllPokemons({ size, offset }) {
    return this.get(`pokemon?limit=${size}&offset=${offset}`)
    
  }
  async getPokemonById({ id }) {
    return this.get(`pokemon/${id}`)
  }
  async getPokemonByName({ name }) {
    return this.get(`pokemon/${name}`)
  }
}

export default PokemonAPI