const { gql } = require('apollo-server');

const typeDefs = gql`
  type PokemonResult {
    name: String
    url: String
    pokemon: Pokemon
  }
  type Pokemon {
    id: Int
    name: String
    weight: Int
    base_experience: Int
    height: Int
    stats: [PokemonStat] 
  }
  type PokemonStat {
    base_stat: Int
    effort: Int
    stat: StatReference
  }

  type StatReference {
    name: String
    url: String
  }
  type PokemonListSearch {
    count: Int
    results: [PokemonResult]
  }

  type Query {
    allPokemons(size: Int, offset: Int): PokemonListSearch
    pokemonById(id: Int!): Pokemon
  }
`;


export default typeDefs