const resolvers = {
  Query: {
    allPokemons: async (_, { size, offset }, { dataSources }) => {
      return dataSources.pokemonAPI.getAllPokemons({ size, offset })
    },
    pokemonById: async (_, { id }, { dataSources }) => {
      return dataSources.pokemonAPI.getPokemonById({ id })
    }
  },
  PokemonResult: {
    pokemon: async ({ name }, _, { dataSources }) => 
      dataSources.pokemonAPI.getPokemonByName({ name })
  }
}


export default resolvers