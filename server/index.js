const { ApolloServer } = require('apollo-server');
import { resolvers, typeDefs, PokemonApi } from './src'

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    pokemonAPI: new PokemonApi(),
  })
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});