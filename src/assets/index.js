import pickUpMarker from './pickUpMarker.svg'
import dropOffMarker from './dropOffMarker.svg'

import dropOffBadgeBlank from './dropOffBadgeBlank.svg'
import dropOffBadgeError from './dropOffBadgeError.svg'
import dropOffBadgePresent from './dropOffBadgePresent.svg'
import pickUpBadgeBlank from './pickUpBadgeBlank.svg'
import pickUpBadgeError from './pickUpBadgeError.svg'
import pickUpBadgePresent from './pickUpBadgePresent.svg'

export const markerIcons = {
  pickUp: pickUpMarker,
  dropOff: dropOffMarker
}

export const inputIcons = {
  pickUp: {
    error: pickUpBadgeError,
    present: pickUpBadgePresent,
    blank: pickUpBadgeBlank
  },
  dropOff: {
    error: dropOffBadgeError,
    present: dropOffBadgePresent,
    blank: dropOffBadgeBlank
  }
}