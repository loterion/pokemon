import { ADD_POKEMON, GET_POKEMON_DETAIL, DELETE_POKEMON } from "../types";

export default function (state, action) {
  const { type, payload } = action;

  switch (type) {
    case ADD_POKEMON:
      return { ...state, pokemons: [...state.pokemons, payload] };
    case DELETE_POKEMON:
      return {
        ...state,
        pokemons: state.pokemons.filter((p) => p.name != payload),
      };
    case GET_POKEMON_DETAIL:
      return { ...state, pokemonDetail: payload };
  }
}
