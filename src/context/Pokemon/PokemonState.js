import React, { useReducer } from "react";
import PokemonReducer from "./PokemonReducer";
import PokemonContext from "./PokemonContext";
import axios from "axios";
import { ADD_POKEMON, DELETE_POKEMON, GET_POKEMON_DETAIL } from "../types";

function PokemonState({ children }) {
  const initalState = {
    pokemons: [],
    pokemonDetail: {},
  };
  const [state, dispatch] = useReducer(PokemonReducer, initalState);
  const addPokemon = (pokemon) => {
    dispatch({
      type: ADD_POKEMON,
      payload: pokemon,
    });
  };
  const deletePokemon = (name) => {
    dispatch({
      type: DELETE_POKEMON,
      payload: name,
    });
  };
  const getPokemonDetail = async (id) => {
    const res = await axios.get(`https://pokeapi.co/api/v2/pokemon/${id}/`);
    dispatch({
      type: GET_POKEMON_DETAIL,
      payload: res.data,
    });
  };
  return (
    <PokemonContext.Provider
      value={{
        pokemons: state.pokemons,
        pokemonDetail: state.pokemonDetail,
        addPokemon,
        getPokemonDetail,
        deletePokemon,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
}

export default PokemonState;
