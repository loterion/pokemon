import React, { useState, useEffect } from "react";

export function useHasFocus(ref) {
  const [focus, setFocus] = useState(true);
  useEffect(() => {
    function handleOutsideClick(event) {
      if (ref.current && !ref.current.contains(event.target)) {
        setFocus(false);
      } else {
        setFocus(true);
      }
    }

    document.addEventListener("mousedown", handleOutsideClick);
    return () => document.removeEventListener("mousedown", handleOutsideClick);
  }, [ref]);

  return focus;
}
