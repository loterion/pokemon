/* This example requires Tailwind CSS v2.0+ */
import React from "react";
import Search from "../Search";

function Header() {
  return (
    <div className="relative bg-white">
      <div className="max-w-7xl mx-auto px-4">
        <div className="flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
          <div className="flex justify-start lg:w-0 lg:flex-1">
            <a href="#">
              <img
                className="hidden sm:flex h-12"
                src="https://c.tenor.com/V4gdku6HgPQAAAAC/pikachu-cute.gif"
                alt=""
              />
            </a>
          </div>

          <div className="flex items-center justify-end md:flex-1 lg:w-0">
            <Search />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
