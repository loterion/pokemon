import React, { useContext } from "react";
import PokemonContext from "../../context/Pokemon/PokemonContext";
import { capitalize } from "../../utils";

function Detail() {
  const { pokemonDetail } = useContext(PokemonContext);
  const { name, base_experience, abilities = [], stats = [] } = pokemonDetail;

  return name !== undefined ? (
    <div className="bg-white shadow-lg rounded-lg overflow-hidden mb-2 p-2 sm:p-8">
      <div className="flex justify-center">
        <img
          alt={name}
          src={`https://img.pokemondb.net/artwork/${name}.jpg`}
          className="focus:outline-none h-64"
        />
      </div>
      <div className="bg-white">
        <div className="sm:p-4">
          <div className="flex items-center">
            <h2 className="focus:outline-none text-2xl font-semibold">
              {name !== undefined && capitalize(name)}
            </h2>
            <p className="focus:outline-none text-xs text-gray-600 pl-5">
              Base experience:{" "}
              <span className="font-bold">{base_experience}</span>
            </p>
          </div>
          <div className="flex flex-col mt-4">
            <h2 className="text-xs font-bold mb-2">Abilities:</h2>
            <div className="flex flex-row overflow-x-auto">
              {abilities.map((a) => (
                <div
                  key={a.ability.name}
                  className="focus:outline-none text-xs text-white px-2 bg-orange-500 py-1 mr-1 rounded"
                >
                  {a.ability.name}
                </div>
              ))}
            </div>
          </div>

          <div className="flex flex-col mt-4">
            <h2 className="text-xs font-bold mb-2">Stats:</h2>
            <div className="flex flex-row overflow-x-auto">
              {stats.map((a) => (
                <div className="focus:outline-none text-xs text-white px-2 bg-fuchsia-600 py-1 mr-1 rounded">
                  {`${a.base_stat} ${a.stat.name}`}
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : null;
}

export default Detail;
