import React from "react";
import Detail from "../Detail";

function Main() {
  return (
    <main role="main" className="flex-grow">
      <Detail />
    </main>
  );
}

export default Main;
