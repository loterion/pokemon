import React from "react";
import ContentLoader from "react-content-loader";

function SuggestionPlaceholder(props) {
  return (
    <div className="p-4">
      <ContentLoader
        speed={2}
        width={300}
        height={230}
        viewBox="0 0 300 230"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
      >
        <circle cx="22" cy="32" r="16" />
        <rect x="50" y="16" rx="0" ry="0" width="300" height="33" />
        <circle cx="22" cy="77" r="16" />
        <rect x="50" y="61" rx="0" ry="0" width="300" height="33" />
        <circle cx="22" cy="122" r="16" />
        <rect x="50" y="106" rx="0" ry="0" width="300" height="33" />
        <circle cx="22" cy="167" r="16" />
        <rect x="50" y="151" rx="0" ry="0" width="300" height="33" />
      </ContentLoader>
    </div>
  );
}

export default SuggestionPlaceholder;
