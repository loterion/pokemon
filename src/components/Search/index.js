import React, { useEffect, useState, useRef } from "react";
import { useQuery, gql } from "@apollo/client";
import { useHasFocus } from "../../utils/hooks";
import Suggestion from "./Suggestion";

export const SEARCH_POKEMON_QUERY = gql`
  query SearchPokemon($text: String!) {
    pokemon_v2_pokemon(where: { name: { _ilike: $text } }) {
      id
      name
    }
  }
`;

function Search() {
  const ref = useRef(null);
  const focus = useHasFocus(ref);
  const [text, setText] = useState("");

  const { data, loading, error, refetch } = useQuery(SEARCH_POKEMON_QUERY, {
    variables: { text: `%${text}%` },
  });

  useEffect(() => {
    if (text.length > 0) refetch();
    if (!focus) setText("");
  }, [text, focus]);

  return (
    <div className="w-72 md:w-96" ref={ref}>
      <input
        className="w-full h-12 px-4 mb-1 text-gray-700 placeholder-gray-400 border rounded-lg"
        type="text"
        placeholder="Search your favorite pokemons..."
        onChange={(e) => setText(e.target.value)}
        value={text}
      />
      {text.length > 0 ? (
        <div className="absolute">
          <Suggestion
            data={
              data !== undefined && data.hasOwnProperty("pokemon_v2_pokemon")
                ? data.pokemon_v2_pokemon
                : []
            }
            isLoading={loading}
            error={error}
          />
        </div>
      ) : null}
    </div>
  );
}

export default Search;
