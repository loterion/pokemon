import React from "react";
import { fireEvent, render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { MockedProvider } from "@apollo/client/testing";
import Search, { SEARCH_POKEMON_QUERY } from "./index";

const mocks = [
  {
    request: {
      query: SEARCH_POKEMON_QUERY,
      variables: {
        text: "%pikachu%",
      },
    },
    result: {
      data: {
        pokemon_v2_pokemon: [
          {
            id: 25,
            name: "pikachu",
            __typename: "pokemon_v2_pokemon",
          },
        ],
      },
    },
  },
];

describe("<Search />", () => {
  let component;

  beforeEach(() => {
    component = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Search />
      </MockedProvider>
    );
  });

  test("renders without errors", () => {
    component.getByPlaceholderText("Search your favorite pokemons...");
  });

  test("show suggestions without erros", () => {
    const input = component.queryByPlaceholderText(
      "Search your favorite pokemons..."
    );

    fireEvent.change(input, { target: { value: "pikachu" } });

    component.getByText("Loading...");
  });
});
