import React from "react";
import SuggestionItem from "./SuggestionItem";
import SuggestionPlaceholder from "./SuggestionPlaceholder";

function Suggestion({ data, isLoading, error }) {
  return (
    <div className="w-72 md:w-96 overflow-auto bg-white shadow-lg rounded-lg max-h-96">
      {isLoading && <SuggestionPlaceholder />}
      {error && <p className="text-red-600 font-bold">{error.message}</p>}
      {data.length > 0
        ? data.map((pokemon) => (
            <SuggestionItem key={`poke${pokemon.id}`} data={pokemon} />
          ))
        : null}
    </div>
  );
}

export default Suggestion;
