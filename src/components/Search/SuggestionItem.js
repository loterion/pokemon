import React, { useContext } from "react";
import PokemonContext from "../../context/Pokemon/PokemonContext";

function SuggestionItem({ data }) {
  const { name } = data;
  const { addPokemon, pokemons } = useContext(PokemonContext);
  const handleClick = () => {
    addPokemon({ name: data.name, id: data.id });
  };
  return (
    <div onClick={handleClick} className="w-full border-b border-gray-100">
      <div className="flex px-6 py-4">
        <img
          className="h-16 mr-4"
          src={`https://img.pokemondb.net/artwork/${name}.jpg`}
          alt=""
        />
        <div className="text-center sm:text-left sm:flex-grow">
          <div className="mb-4">
            <p className="text-xl leading-tight">{name}</p>
          </div>
          <div>
            {pokemons.find((p) => p.name === data.name) ? (
              <button
                disabled
                className="text-xs font-semibold rounded-full px-4 py-1 leading-normal bg-gray-300 border"
              >
                Added
              </button>
            ) : (
              <button className="text-xs font-semibold rounded-full px-4 py-1 leading-normal bg-white border">
                Add
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SuggestionItem;
