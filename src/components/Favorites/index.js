import React, { useContext } from "react";
import FavoriteItem from "./FavoriteItem";
import PokemonContext from "../../context/Pokemon/PokemonContext";

function Favorites() {
  const { pokemons } = useContext(PokemonContext);
  return (
    <div className="flex flex-col">
      <h2 className="focus:outline-none text-2xl font-semibold mb-4">
        Favorites
      </h2>
      {pokemons.length ? null : (
        <span className="font-semibold text-gray-400">
          No pokemons selected yet
        </span>
      )}
      <div>
        {pokemons.map((pokemon) => (
          <FavoriteItem key={pokemon.name} data={pokemon} />
        ))}
      </div>
    </div>
  );
}

export default Favorites;
