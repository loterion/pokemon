import React, { useContext } from "react";
import PokemonContext from "../../context/Pokemon/PokemonContext";
import { capitalize } from "../../utils";

const FavoriteItem = ({ data }) => {
  const { name } = data;
  const { getPokemonDetail, deletePokemon } = useContext(PokemonContext);
  return (
    <div className="bg-white shadow-lg rounded-lg overflow-hidden mb-4">
      <div className="flex items-center px-6 py-4">
        <img
          className="w-16 mr-4"
          src={`https://img.pokemondb.net/artwork/${name}.jpg`}
          alt=""
        />

        <div className="text-center sm:text-left sm:flex-grow">
          <p className="mb-2 text-xl leading-tight font-semibold">
            {capitalize(name)}
          </p>
          <button
            onClick={() => getPokemonDetail(name)}
            className="text-xs font-semibold rounded-full px-4 py-1 leading-normal bg-white border mr-2"
          >
            Info
          </button>
          <button
            onClick={() => deletePokemon(name)}
            className="text-xs text-pink-600 font-semibold rounded-full px-4 py-1 leading-normal bg-white border border-pink-600"
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
};

export default FavoriteItem;
