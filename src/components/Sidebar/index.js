import React from "react";
import Favorites from "../Favorites";

function Sidebar() {
  return (
    <aside className="w-full md:w-1/3">
      <div className="top-0 sm:p-4 w-full">
        <Favorites />
      </div>
    </aside>
  );
}

export default Sidebar;
