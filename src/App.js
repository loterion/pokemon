import React from "react";
import PokemonState from "./context/Pokemon/PokemonState";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";
import Main from "./components/Main";

function App() {
  return (
    <PokemonState>
      <Header />
      <div className="max-w-7xl mx-auto px-4 flex flex-col sm:flex-row flex-grow overflow-hidden mt-8">
        <Sidebar />
        <Main />
      </div>
    </PokemonState>
  );
}

export default App;
