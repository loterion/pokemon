export const pokemons = {
  data: {
    pokemon_v2_pokemon: [
      {
        id: 25,
        name: "pikachu",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10080,
        name: "pikachu-rock-star",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10081,
        name: "pikachu-belle",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10082,
        name: "pikachu-pop-star",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10083,
        name: "pikachu-phd",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10084,
        name: "pikachu-libre",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10085,
        name: "pikachu-cosplay",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10094,
        name: "pikachu-original-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10095,
        name: "pikachu-hoenn-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10096,
        name: "pikachu-sinnoh-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10097,
        name: "pikachu-unova-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10098,
        name: "pikachu-kalos-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10099,
        name: "pikachu-alola-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10148,
        name: "pikachu-partner-cap",
        __typename: "pokemon_v2_pokemon",
      },
      {
        id: 10190,
        name: "pikachu-gmax",
        __typename: "pokemon_v2_pokemon",
      },
    ],
  },
};
