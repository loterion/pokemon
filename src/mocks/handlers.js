import { graphql } from "msw";
import { pokemons } from "../../mocks/data";

export const handlers = [
  graphql.query("SearchPokemon", (req, res, ctx) => {
    return res(ctx.data(pokemons));
  }),
];
