## Netquest Frontend Task

We have a Pokédex application that started as a proof of concept but needs to be finished and be production ready.

The application should:

- Show total stats (Health, Attack, Defense, Speed) of the selected Pokémon.
- Avoid selecting the same Pokémon twice.
- Allow Pokémon search by name.
- Not have styling issues.

You will see that most of the code is already done, but it may require some improvement or refactor. Also, you may want to improve the existing state management.

The app reads from a public REST API, but you have a GraphQL proxy at your disposition in case you want to use it.

Whether you choose REST or GraphQL, we will NOT take this into account. We will only check your implementation of the chosen option.

You can add whichever library you may find necessary, considering the already existing ones. 

Enjoy!
